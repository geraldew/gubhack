# Gubhack

gubhack = Gratuitous Ubuntu Hack 

This is a python program providing some customisations for Ubuntu installations for things are annoying to see manually.

## current status

Current status: barely begun 

Being constructed as two files:
- _lib to provide the required features
- _gui to allow pointy-clicky selections and actions.

## Why write this?

To scratch the itch of various settings that are clearly possible but not elsewhere made easy to enact.

Currently it is aimed at providing a clearer way to control the options and settings for:
- Display Manager - i.e. the usual login window on a Linux user computer 
- optional settings for several specific Display Managers

As these featured will usually involve actions by the system administrator, the aim here is streamlined convenience of existing options - rather than extension of those options.

The idea is provide safe and easy settings for the novice administrator. The intent is to be simple and clear rather than sophisticated and comprehensive.

## Features

### Display Managers

For most Linux setups aimed at GUI users, there is a "Display Manager" which presents a login screen and when users log in, starts up a session in the Desktop Environment of their choice.

#### Greeters for the LightDM Display Manager

For the Display Manager: LightDM the various optional display and functional arrangements are known as "Greeters".

- Showing the available Greeters
- Installing Greeters
- Showing the installed Greeters
- Imposing Greeters

#### Themes for the SDDM Display Manager

For the Display Manager: SDDM the various optional display and functional arrangements are known as "Themes".

- Showing the available Themes
- Installing Themes
- Showing the installed Themes
- Imposing Themes

## Under the hood

The settings being manipulated by this tool will be combinations of:
- Debian package installations - i.e. dpkg and apt
- inspection of and changes to settings in the /etc folder
- other custom methods as required (by how others things are setup).

## Other candidate features

Some ideas of things not currently implemented but 

### Desktop Environments

This might just be a matter of having a curated list of the "*buntu-desktop" metapackages - so that a novice admin doesn't have to guess what those might be.

e.g.
- Xubutu / Xfce
- Budgie
- Kubuntu

### Window Managers

Doubtful but could be considered if there's some merit in handling these independent of the DE's that usually use them.

### Video Drivers

Two possible reasons for covering this might be:
- making it easier to see what the current drivers are, and change them
- repairing problematic drivers - presumably as a line command option of some sort

### Booting options

- perhaps if there are some gaps between the various existing tools, or maybe just to ease installing those in preparation of problems:
- e.g. rEFInd, Boot Repairer, Grub Customiser etc
- especially where some of those are reliable but not in the stock Ubuntu repositories

