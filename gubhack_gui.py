#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# separate file for designing the gubhack GUI 

# Copyright (C) 2018-2020  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys
import time

import tkinter as tkntr 
import tkinter.ttk as tkntr_ttk 
from tkinter import filedialog
from tkinter import messagebox 

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

sys.path.append('./')
import gubhack_lib as gbhcklb

# --------------------------------------------------
# Support Stock - just useful generic things
# --------------------------------------------------

# for use in making a date and time based filename
def get_datetime_str():
	return time.strftime("%Y_%m_%d_%H_%M_%S", time.gmtime())

# for use in making a date and time based filename
def get_nowtime_str():
	return time.strftime("%H_%M_%S", time.gmtime())

def getpathref_fromuser( show_title, from_folder_name) :
	if len(show_title) == 0 :
		show_title = "Select a Folder"
	if len(from_folder_name) == 0 :
		from_folder_name = "/"
	root = tkntr.Tk()
	root.withdraw()
	FolderName = filedialog.askdirectory( title=show_title, initialdir=from_folder_name)
	return FolderName

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def app_code_name():
	return "gubhack_gui"

def app_code_abbr():
	return "gbhckui"

# --------------------------------------------------
# Features - i.e. requiring awareness of custom structures
# --------------------------------------------------

def GubHackWindow( p_window_context) : 
	# feature action defs 
	def actionlist_add( p_str) :
		b_actnlg_lb_log.insert( 0, get_nowtime_str() + " " + p_str)
	# feature gui event defs
	# - Display Managers
	def on_b_dm_button_find_now_dm() :
		actionlist_add( "Calling Find_Current_DisplayManager()")
		now_dm = gbhcklb.Find_Current_DisplayManager()
		if len( now_dm ) > 0 :
			b_dm_label_found_now_dm.configure( text = now_dm)
			actionlist_add( "Found: " + now_dm)
	def on_b_dm_button_find_dm_avail() :
		actionlist_add( "Calling gbhcklb.Find_Available_DisplayManagers()")
		lst_dm = gbhcklb.Find_Available_DisplayManagers()
		if len(lst_dm) > 0 :
			b_dm_cb_dm_avail['values'] = tuple( lst_dm )
			actionlist_add( "Found " + str(len(lst_dm)) + " available display managers. Have been put into drop-down list.")
	def on_b_dm_button_dm_avail_install() :
		dm_to_set = b_dm_str_dm_select.get()
		if len( dm_to_set) > 0 :
			actionlist_add( "Calling Impose_DisplayManager with (" + dm_to_set + ")")
			did_dm = gbhcklb.Impose_DisplayManager( dm_to_set )
			actionlist_add( "Impose_DisplayManager success: " + str(did_dm )) 
		else :
			actionlist_add( "No Display Manager to set!")
	def on_b_dm_button_find_dm_installed() :
		actionlist_add( "Calling gbhcklb.Find_Installed_DisplayManagers()")
		lst_dm = gbhcklb.Find_Installed_DisplayManagers()
		if len(lst_dm) > 0 :
			b_dm_cb_dm_installed['values'] = tuple( lst_dm )
			actionlist_add( "Found " + str(len(lst_dm)) + " installed display managers. Have been put into drop-down list.")
	def on_b_dm_button_dm_installed_impose() :
		dm_to_set = b_dm_str_dm_installed_select.get()
		if len( dm_to_set) > 0 :
			actionlist_add( "Calling Impose_DisplayManager with (" + dm_to_set + ")")
			did_dm = gbhcklb.Impose_DisplayManager( dm_to_set )
			actionlist_add( "Impose_DisplayManager success: " + str(did_dm )) 
		else :
			actionlist_add( "No Display Manager to set!")
	# - LightDM Greeters
	def on_c_lghtdm_button_find_now_dm() :
		actionlist_add( "Calling Find_Current_LightDM_Greeter")
		str_lghtdm_grtr = gbhcklb.Find_Current_LightDM_Greeter()
		if len(str_lghtdm_grtr) > 0 :
			c_lghtdm_label_found_now_dm.configure( text = str_lghtdm_grtr)
			actionlist_add( "Found " + str_lghtdm_grtr + " as current LightDM Greeter.")
	def on_c_lghtdm_button_grtr_avail_find() :
		actionlist_add( "Calling Find_Installed_LightDM_Greeters")
		lst_lghtdm_grtrs = gbhcklb.Find_Available_LightDM_Greeters()
		if len(lst_lghtdm_grtrs) > 0 :
			c_lghtdm_cb_grtr_avail['values'] = tuple( lst_lghtdm_grtrs )
			actionlist_add( "Found " + str(len(lst_lghtdm_grtrs)) + " installed display managers. Have been put into drop-down list.")
	def on_c_lghtdm_button_grtr_avail_install() :
		greeter_to_set = c_lghtdm_str_grtr_avail_select.get()
		if len( greeter_to_set) > 0 :
			actionlist_add( "Calling Impose_LightDM_Greeter with (" + greeter_to_set + ")")
			did_grtr = gbhcklb.Install_LightDM_Greeter( greeter_to_set )
			actionlist_add( "Impose_LightDM_Greeter success: " + str(did_grtr )) 
		else :
			actionlist_add( "No Display Manager to set!")
	def on_c_lghtdm_button_grtr_installed_find() :
		actionlist_add( "Calling Find_Installed_LightDM_Greeters")
		lst_lghtdm_grtrs = gbhcklb.Find_Installed_LightDM_Greeters()
		if len(lst_lghtdm_grtrs) > 0 :
			c_lghtdm_cb_grtr_installed['values'] = tuple( lst_lghtdm_grtrs )
			actionlist_add( "Found " + str(len(lst_lghtdm_grtrs)) + " installed display managers. Have been put into drop-down list.")
	def on_c_lghtdm_button_grtr_installed_impose() :
		greeter_to_set = c_lghtdm_str_grtr_installed_select.get()
		if len( greeter_to_set) > 0 :
			actionlist_add( "Calling Impose_LightDM_Greeter with (" + greeter_to_set + ")")
			did_grtr = gbhcklb.Impose_LightDM_Greeter( greeter_to_set )
			actionlist_add( "Impose_LightDM_Greeter success: " + str(did_grtr )) 
		else :
			actionlist_add( "No Display Manager to set!")
	# - SDDM Themes
	def on_c_sddm_button_find_themes() :
		actionlist_add( "Calling Find_Available_SDDM_Themes")
		lst_sddm_themes = gbhcklb.Find_Available_SDDM_Themes()
	def on_c_sddm_button_set_theme() :
		theme_to_set = c_sddm_str_thm_select.get()
		if len( theme_to_set) > 0 :
			actionlist_add( "Calling Impose_SDDM_Theme with (" + theme_to_set + ")")
			did_theme = gbhcklb.Impose_SDDM_Theme( theme_to_set )
			actionlist_add( "Impose_SDDM_Theme success: " + str(did_theme )) 
		else :
			actionlist_add( "No SDDM Theme to set!")
	def on_c_sddm_button_find_now_dm() :
		actionlist_add( "Calling Find_Current_SDDM_Theme")
		str_sddm_thm = gbhcklb.Find_Current_SDDM_Theme()
		if len(str_sddm_thm) > 0 :
			c_sddm_label_found_now_dm.configure( text = str_sddm_thm)
			actionlist_add( "Found " + str_sddm_thm + " as current SDDM Theme.")
	def on_c_sddm_button_thm_avail_find() :
		actionlist_add( "Calling Find_Available_SDDM_Themes")
		lst_sddm_thms = gbhcklb.Find_Available_SDDM_Themes()
		if len(lst_sddm_thms) > 0 :
			c_sddm_cb_thm_avail['values'] = tuple( lst_sddm_thms )
			actionlist_add( "Found " + str(len(lst_sddm_thms)) + " installed SDDM Themes. Have been put into drop-down list.")
	def on_c_sddm_button_thm_avail_install() :
		greeter_to_set = c_sddm_str_thm_avail_select.get()
		if len( greeter_to_set) > 0 :
			actionlist_add( "Calling Install_SDDM_Theme with (" + greeter_to_set + ")")
			did_thm = gbhcklb.Install_SDDM_Theme( greeter_to_set )
			actionlist_add( "Install_SDDM_Theme success: " + str(did_thm )) 
		else :
			actionlist_add( "No SDDM Theme to set!")
	def on_c_sddm_button_thm_installed_find() :
		actionlist_add( "Calling Find_Installed_SDDM_Themes")
		lst_sddm_thms = gbhcklb.Find_Installed_SDDM_Themes()
		if len(lst_sddm_thms) > 0 :
			c_sddm_cb_thm_installed['values'] = tuple( lst_sddm_thms )
			actionlist_add( "Found " + str(len(lst_sddm_thms)) + " installed SDDM Themes. Have been put into drop-down list.")
	def on_c_sddm_button_thm_installed_impose() :
		greeter_to_set = c_sddm_str_thm_installed_select.get()
		if len( greeter_to_set) > 0 :
			actionlist_add( "Calling Impose_SDDM_Theme with (" + greeter_to_set + ")")
			did_thm = gbhcklb.Impose_SDDM_Theme( greeter_to_set )
			actionlist_add( "Impose_SDDM_Theme success: " + str(did_thm )) 
		else :
			actionlist_add( "No SDDM Theme to set!")
	# actions
	def on_a_button_exit() :
		p_window_context.quit()
	# :::::::::::::::::::::::::::::::::::::
	# main for GubHackWindow
	# ---------1 Frame: Heading
	# Have a heading section - outside the main Tabbed interface
	a_frame_heading = tkntr.Frame( p_window_context)
	a_frame_heading.pack( fill = tkntr.X)
	# .........1 Heading content
	a_button_exit = tkntr.Button( a_frame_heading, text="(-)", command=on_a_button_exit)
	a_label_title = tkntr.Label( a_frame_heading, text="GubHack")
	label_explain = tkntr.Label( a_frame_heading, text="A tool for Gratuitous Ubuntu Hacking")
	a_button_exit.pack( side=tkntr.LEFT, padx=8, pady=8)
	a_label_title.pack( side = tkntr.LEFT, padx=8, pady=8)
	label_explain.pack( side = tkntr.RIGHT, padx=8, pady=8)
	# ::::::::::1 Make the main notebook i.e. tabbed interface
	a_nb = tkntr.ttk.Notebook( p_window_context)
	a_nb.pack( expand=1, fill=tkntr.BOTH)
	# ==========1 Tab: Display Managers
	a_nbtab_dm = tkntr.Frame( a_nb)
	a_nb.add( a_nbtab_dm, text="Display Managers")
	# ----------2 Frame: Have a top section 
	b_dm_frame_1 = tkntr.Frame( a_nbtab_dm)
	b_dm_frame_1.pack( fill = tkntr.X)
	b_dm_label_title = tkntr.Label( b_dm_frame_1, text="Display Manager Hacks")
	b_dm_label_explain = tkntr.Label( b_dm_frame_1, text="Various hacks")
	b_dm_label_title.pack( side = tkntr.LEFT, padx=8, pady=8)
	b_dm_label_explain.pack( side = tkntr.RIGHT, padx=8, pady=8)
	# ----------2 Have a controls section outside the Tabbed interface
	b_dm_frame_2 = tkntr.Frame( a_nbtab_dm)
	b_dm_frame_2.pack( fill = tkntr.X)
	b_dm_label_intent = tkntr.Label( b_dm_frame_2, text="Display Manager Controls")
	b_dm_label_reason = tkntr.Label( b_dm_frame_2, text="Select Display Manager")
	b_dm_label_intent.pack( side = tkntr.LEFT, padx=8, pady=8)
	b_dm_label_reason.pack( side = tkntr.RIGHT, padx=8, pady=8)
	# ----------2 frame for check current Display Manager
	b_dm_frame_3 = tkntr.Frame( a_nbtab_dm)
	b_dm_frame_3.pack( fill = tkntr.X)
	b_dm_button_find_now_dm = tkntr.Button( b_dm_frame_3, text="Check current Display Manager", command=on_b_dm_button_find_now_dm)
	b_dm_label_found_now_dm = tkntr.Label( b_dm_frame_3, text="Not yet checked")
	b_dm_button_find_now_dm.pack( side=tkntr.LEFT, padx=8, pady=8)
	b_dm_label_found_now_dm.pack( side = tkntr.LEFT, padx=8, pady=8)
	# ----------2 frame for installing a Display Manager
	b_dm_frame_4 = tkntr.Frame( a_nbtab_dm)
	b_dm_button_find_avail_dm = tkntr.Button( b_dm_frame_4, text="Check available Display managers", command=on_b_dm_button_find_dm_avail)
	b_dm_frame_4.pack( fill = tkntr.X)
	b_dm_str_dm_avail_select = tkntr.StringVar() 
	b_dm_cb_dm_avail = tkntr.ttk.Combobox( b_dm_frame_4, width = 40, textvariable = b_dm_str_dm_avail_select) 
	b_dm_cb_dm_avail['values'] = ( "Not-yet-checked!")
	b_dm_button_dm_avail_install = tkntr.Button( b_dm_frame_4, text="Install selected Display manager", command=on_b_dm_button_dm_avail_install)
	# .......... packing
	b_dm_button_find_avail_dm.pack( side=tkntr.LEFT, padx=8, pady=8)
	b_dm_cb_dm_avail.pack( side = tkntr.LEFT)
	b_dm_button_dm_avail_install.pack( side=tkntr.RIGHT, padx=8, pady=8)
	# ----------2 frame for imposing a Display Manager
	b_dm_frame_5 = tkntr.Frame( a_nbtab_dm)
	b_dm_button_find_dm_installed = tkntr.Button( b_dm_frame_5, text="Check installed Display managers", command=on_b_dm_button_find_dm_installed)
	b_dm_frame_5.pack( fill = tkntr.X)
	b_dm_str_dm_installed_select = tkntr.StringVar() 
	b_dm_cb_dm_installed = tkntr.ttk.Combobox( b_dm_frame_5, width = 40, textvariable = b_dm_str_dm_installed_select) 
	b_dm_cb_dm_installed['values'] = ( "Not-yet-checked!")
	b_dm_button_dm_installed_impose = tkntr.Button( b_dm_frame_5, text="Impose selection as Display manager", command=on_b_dm_button_dm_installed_impose)
	# .......... packing
	b_dm_button_find_dm_installed.pack( side=tkntr.LEFT, padx=8, pady=8)
	b_dm_cb_dm_installed.pack( side = tkntr.LEFT, fill = tkntr.X)
	b_dm_button_dm_installed_impose.pack( side=tkntr.RIGHT, padx=8, pady=8)
	# ----------2 Frame: for the notebook per Display Manager
	b_dm_frame_nb_perdm = tkntr.Frame( a_nbtab_dm)
	b_dm_frame_nb_perdm.pack( fill = tkntr.X)
	# ::::::::::2 Notebook: per Display Manager
	b_dm_nb_perdm = tkntr.ttk.Notebook( b_dm_frame_nb_perdm)
	b_dm_nb_perdm.pack( expand=1, fill=tkntr.BOTH)
	# ==========2 Tab: for the LightDM tab
	b_dm_nbab_lghtdm = tkntr.Frame(b_dm_nb_perdm)
	b_dm_nb_perdm.add( b_dm_nbab_lghtdm, text="LightDM Greeter")
	# ---------- Frame: LightDM Heading
	c_lghtdm_frame_1 = tkntr.Frame( b_dm_nbab_lghtdm)
	c_lghtdm_frame_1.pack( fill = tkntr.X)
	c_lghtdm_label_heading = tkntr.Label( c_lghtdm_frame_1, text="LightDM Greeters")
	c_lghtdm_label_heading.pack( side = tkntr.LEFT, padx=8, pady=8)
	# ----------2 frame for check current LightDM Greeter
	c_lghtdm_frame_2 = tkntr.Frame( b_dm_nbab_lghtdm)
	c_lghtdm_frame_2.pack( fill = tkntr.X)
	# .......... create components
	c_lghtdm_button_find_now_dm = tkntr.Button( c_lghtdm_frame_2, text="Check current LightDM Greeter", command=on_c_lghtdm_button_find_now_dm)
	c_lghtdm_label_found_now_dm = tkntr.Label( c_lghtdm_frame_2, text="Not yet checked")
	c_lghtdm_button_find_now_dm.pack( side=tkntr.LEFT, padx=8, pady=8)
	c_lghtdm_label_found_now_dm.pack( side = tkntr.LEFT, padx=8, pady=8)
	# ---------- Frame: LightDM Available
	c_lghtdm_frame_3 = tkntr.Frame( b_dm_nbab_lghtdm)
	c_lghtdm_frame_3.pack( fill = tkntr.X)
	# .......... create
	c_lghtdm_button_grtr_avail_find = tkntr.Button( c_lghtdm_frame_3, text="Check available", command=on_c_lghtdm_button_grtr_avail_find)
	c_lghtdm_str_grtr_avail_select = tkntr.StringVar() 
	c_lghtdm_cb_grtr_avail = tkntr.ttk.Combobox( c_lghtdm_frame_3, width =60, textvariable = c_lghtdm_str_grtr_avail_select) 
	c_lghtdm_cb_grtr_avail['values'] = ( "Not-yet-checked!")
	c_lghtdm_button_grtr_avail_install = tkntr.Button( c_lghtdm_frame_3, text="Install selected Greeter", command=on_c_lghtdm_button_grtr_avail_install)
	# .......... packing
	c_lghtdm_button_grtr_avail_find.pack( side=tkntr.LEFT, padx=8, pady=8)
	c_lghtdm_cb_grtr_avail.pack( side = tkntr.LEFT)
	c_lghtdm_button_grtr_avail_install.pack( side=tkntr.RIGHT, padx=8, pady=8)
	# ---------- Frame: LightDM Installed
	c_lghtdm_frame_4 = tkntr.Frame( b_dm_nbab_lghtdm)
	c_lghtdm_frame_4.pack( fill = tkntr.X)
	# .......... create
	c_lghtdm_button_grtr_installed_find = tkntr.Button( c_lghtdm_frame_4, text="Check installed LightDM Greeters", command=on_c_lghtdm_button_grtr_installed_find)
	c_lghtdm_str_grtr_installed_select = tkntr.StringVar() 
	c_lghtdm_cb_grtr_installed = tkntr.ttk.Combobox( c_lghtdm_frame_4, width = 40, textvariable = c_lghtdm_str_grtr_installed_select) 
	c_lghtdm_cb_grtr_installed['values'] = ( "Not-yet-checked!")
	c_lghtdm_button_grtr_installed_impose = tkntr.Button( c_lghtdm_frame_4, text="Impose selected Greeter", command=on_c_lghtdm_button_grtr_installed_impose)
	# .......... packing
	c_lghtdm_button_grtr_installed_find.pack( side=tkntr.LEFT, padx=8, pady=8)
	c_lghtdm_cb_grtr_installed.pack( side = tkntr.LEFT)
	c_lghtdm_button_grtr_installed_impose.pack( side=tkntr.RIGHT, padx=8, pady=8)
	# ==========2 Make and add the SDDM tab
	b_dm_nbab_sddm = tkntr.Frame(b_dm_nb_perdm)
	b_dm_nb_perdm.add( b_dm_nbab_sddm, text="SDDM Theme")
	# ---------- Frame: SDDM Heading
	c_sddm_frame_1 = tkntr.Frame( b_dm_nbab_sddm)
	c_sddm_frame_1.pack( fill = tkntr.X)
	c_sddm_label_heading = tkntr.Label( c_sddm_frame_1, text="SDDM Themes")
	c_sddm_label_heading.pack( side = tkntr.LEFT, padx=8, pady=8)
	# ----------2 frame for check current SDDM Greeter
	c_sddm_frame_2 = tkntr.Frame( b_dm_nbab_sddm)
	c_sddm_frame_2.pack( fill = tkntr.X)
	# .......... create components
	c_sddm_button_find_now_dm = tkntr.Button( c_sddm_frame_2, text="Check current SDDM Theme", command=on_c_sddm_button_find_now_dm)
	c_sddm_label_found_now_dm = tkntr.Label( c_sddm_frame_2, text="Not yet checked")
	c_sddm_button_find_now_dm.pack( side=tkntr.LEFT, padx=8, pady=8)
	c_sddm_label_found_now_dm.pack( side = tkntr.LEFT, padx=8, pady=8)
	# ---------- Frame: SDDM Available
	c_sddm_frame_3 = tkntr.Frame( b_dm_nbab_sddm)
	c_sddm_frame_3.pack( fill = tkntr.X)
	# .......... create
	c_sddm_button_thm_avail_find = tkntr.Button( c_sddm_frame_3, text="Check available", command=on_c_sddm_button_thm_avail_find)
	c_sddm_str_thm_avail_select = tkntr.StringVar() 
	c_sddm_cb_thm_avail = tkntr.ttk.Combobox( c_sddm_frame_3, width =60, textvariable = c_sddm_str_thm_avail_select) 
	c_sddm_cb_thm_avail['values'] = ( "Not-yet-checked!")
	c_sddm_button_thm_avail_install = tkntr.Button( c_sddm_frame_3, text="Install selected Theme", command=on_c_sddm_button_thm_avail_install)
	# .......... packing
	c_sddm_button_thm_avail_find.pack( side=tkntr.LEFT, padx=8, pady=8)
	c_sddm_cb_thm_avail.pack( side = tkntr.LEFT)
	c_sddm_button_thm_avail_install.pack( side=tkntr.RIGHT, padx=8, pady=8)
	# ---------- Frame: SDDM Installed
	c_sddm_frame_4 = tkntr.Frame( b_dm_nbab_sddm)
	c_sddm_frame_4.pack( fill = tkntr.X)
	# .......... create
	c_sddm_button_thm_installed_find = tkntr.Button( c_sddm_frame_4, text="Check installed SDDM Themes", command=on_c_sddm_button_thm_installed_find)
	c_sddm_str_thm_installed_select = tkntr.StringVar() 
	c_sddm_cb_thm_installed = tkntr.ttk.Combobox( c_sddm_frame_4, width = 40, textvariable = c_sddm_str_thm_installed_select) 
	c_sddm_cb_thm_installed['values'] = ( "Not-yet-checked!")
	c_sddm_button_thm_installed_impose = tkntr.Button( c_sddm_frame_4, text="Impose selected Theme", command=on_c_sddm_button_thm_installed_impose)
	# .......... packing
	c_sddm_button_thm_installed_find.pack( side=tkntr.LEFT, padx=8, pady=8)
	c_sddm_cb_thm_installed.pack( side = tkntr.LEFT)
	c_sddm_button_thm_installed_impose.pack( side=tkntr.RIGHT, padx=8, pady=8)
	# ==========1 Tab: for Desktop Environments
	a_nbtab_deskenvs = tkntr.Frame(a_nb)
	a_nb.add( a_nbtab_deskenvs, text="Desktop Environments")
	# ----------2 Frame: Desktop Environments Heading
	b_frame_de_heading = tkntr.Frame( a_nbtab_deskenvs)
	b_frame_de_heading.pack( fill = tkntr.X)
	b_label_de_heading = tkntr.Label( b_frame_de_heading, text="Desktop Environment Conrols")
	b_label_de_heading.pack( side=tkntr.LEFT, fill=tkntr.X, expand=True)
	# ==========1 Tab: for Action Log
	a_nbtab_actionlog = tkntr.Frame(a_nb)
	a_nb.add( a_nbtab_actionlog, text="Action logs")
	# ----------2 Frame: Action Log Heading
	b_frame_al_heading = tkntr.Frame( a_nbtab_actionlog)
	b_frame_al_heading.pack( fill = tkntr.X)
	b_label_al_heading = tkntr.Label( b_frame_al_heading, text="Action log - most recent at top.")
	b_label_al_heading.pack( side=tkntr.LEFT, fill=tkntr.X, expand=True)
	# ----------2 Frame: Action Log list
	b_frame_al_listing = tkntr.Frame( a_nbtab_actionlog)
	b_frame_al_listing.pack( fill = tkntr.BOTH, expand=1)
	# ..........
	b_actnlg_lb_log = tkntr.Listbox( b_frame_al_listing, height=10 )
	b_actnlg_lb_log.pack( side = tkntr.LEFT, fill=tkntr.BOTH, expand=1)

# Just an abstraction just to not be in main
def Abstraction_From_Main() :
	root = tkntr.Tk()
	root.title("GubHack Tabbed Interface")
	root.wm_geometry("900x600")
	GubHackWindow(root)
	root.mainloop()

if __name__ == "__main__" :
	print( "Test development of a window interface for GubHack")
	Abstraction_From_Main()
	print( "Test run done.")
