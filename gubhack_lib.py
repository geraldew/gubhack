#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Python module for actions needed by the gubhack GUI 

# Copyright (C) 2018-2020  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

#import sys
import os
#import os.path as osp

import subprocess

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def app_code_name():
	return "gubhack_lib"

def app_code_abbr():
	return "gbhcklb"

# --------------------------------------------------
# Features - i.e. requiring awareness of custom structures
# --------------------------------------------------

# Ubuntu package installation

def Ubuntu_Package_Install( p_str_packagename ):
	# sudo apt-get install [displaymanagername]
	return False

def Get_CommandLine_Response( p_str_commandline ):
	#print( "Get_CommandLine_Response:")
	#print( p_str_commandline)
	task = subprocess.Popen( p_str_commandline, shell=True, stdout=subprocess.PIPE)
	data = task.stdout.read()
	#assert task.wait() == 0
	lst_lines = str(data.decode('utf-8')).split("\n") 
	return lst_lines

def Get_Packages_Available_Name_Filter( p_str_namefilter ):
	# dreadful coding for now, needs complete overhaul 
	str_commandline = "apt-cache search " + p_str_namefilter + " --names-only"
	lst_gotback = Get_CommandLine_Response( str_commandline )
	lst_answer = []
	for lin in lst_gotback :
		sublin = lin.strip()
		if len(sublin) > 0 :
			lst_answer.append(sublin)
	return lst_answer

def Get_Packages_Installed_Providing( p_str_provfilter ):
	# dreadful coding for now, needs complete overhaul 
	str_commandline = "dpkg-query -W -f='Package: ${Package}\nProvides: ${Provides}\n' | grep -B 1 -E " \
		+ '"^Provides: ' + p_str_provfilter + '"'
	lst_gotback = Get_CommandLine_Response( str_commandline )
	lst_answer = []
	sentinel = "Package:"
	for lin in lst_gotback :
		if sentinel in lin :
			sublst = lin.split( sentinel)
			for sub in sublst :
				sublin = sub.strip()
				if len(sublin) > 0 :
					lst_answer.append(sublin)
	return lst_answer
	
# Display Managers

def Find_Current_DisplayManager():
	# return a string
	# cat /etc/X11/default-display-manager
	lst_crrnt_dm = Get_CommandLine_Response( "cat /etc/X11/default-display-manager" )
	if len(lst_crrnt_dm) > 0 :
		crrnt_dm = lst_crrnt_dm[0]
	else :
		crrnt_dm = ""
	return crrnt_dm

def Find_Available_DisplayManagers():
	# return a list
	str_cmd = "dpkg-query -W -f='Package: ${Package}\nProvides: ${Provides}\n' | grep -B 1 -E " \
		+ '"^Provides: x-display-manager"'
	# dpkg-query -W -f='Package: ${Package}\nProvides: ${Provides}\n' | grep -B 1 -E "^Provides: x-display-manager"
	lst_dm = [ "LightDM", "SDDM", "GDM" ]
	return lst_dm

def Find_Installed_DisplayManagers():
	# return a list
	lst_dm = Get_Packages_Installed_Providing( "x-display-manager" )
	return lst_dm

def Install_DisplayManager( p_str_DisplayManager ):
	# make ready for being imposed
	return False

def Impose_DisplayManager( p_str_DisplayManager ):
	# set nominated as current
	return False

# LightDM Greeters

def Find_Current_LightDM_Greeter():
	# return a string
	if False :
		# first attempt
		# cat /etc/lightdm/lightdm.conf | head
		lst_crrnt_lightdm = Get_CommandLine_Response( "cat /etc/lightdm/lightdm.conf | head" )
		if len(lst_crrnt_lightdm) > 0 :
			str_crrnt_lightdm = lst_crrnt_lightdm[0]
		else :
			str_crrnt_lightdm = ""
	else :
		# second attempt, see https://askubuntu.com/questions/870054/how-to-determine-which-lightdm-greeter-is-in-use
		# lightdm --show-config |& awk -F= '/greeter-session=/{print $2}'
		# lightdm --show-config |& grep -B 1 -E ".*greeter-session.*"| tail --lines=1
		lst_crrnt_lightdm = Get_CommandLine_Response( "bash -c '" + 'lightdm --show-config |& grep -B 1 -E ".*greeter-session.*"| tail --lines=1' + "'")
		if len(lst_crrnt_lightdm) > 0 :
			str_crrnt_lightdm = lst_crrnt_lightdm[0]
		else :
			str_crrnt_lightdm = ""
	return str_crrnt_lightdm

def Find_Available_LightDM_Greeters():
	# return a list
	lst_greeters = Get_Packages_Available_Name_Filter( "greeter" )
	return lst_greeters

def Find_Installed_LightDM_Greeters():
	# return a list
	lst_dm = Get_Packages_Installed_Providing( ".*lightdm.*greeter.*" )
	return lst_dm

def Install_LightDM_Greeter( p_str_Greeter ):
	# set nominated as current
	return False

def Impose_LightDM_Greeter( p_str_Greeter ):
	# set nominated as current
	return False

# SDDM Themes

def Find_Current_SDDM_Theme():
	# return a string
	# read the file /etc/sddm.conf 
	# return the line containing Current=
	str_commandline = "cat /etc/sddm.conf | grep 'Current='"
	#str_commandline = "cat /etc/sddm.conf"
	str_rspns = ""
	lst_rspns = Get_CommandLine_Response( str_commandline )
	#print( lst_rspns )
	if len(lst_rspns) > 0 :
		str_rspns = lst_rspns[0].strip()
	if len( str_rspns) == 0 :
		# assume that no /etc/sddm.conf file means there's only one theme so no need for a setting
		str_commandline = "ls /usr/share/sddm/themes"
		lst_rspns = Get_CommandLine_Response( str_commandline )
		if len(lst_rspns) > 0 :
			str_rspns = lst_rspns[0].strip()
		else :
			str_rspns = ""
	return str_rspns

def Find_Available_SDDM_Themes():
	# return a list
	lst_themes = Get_Packages_Available_Name_Filter( "sddm" )
	return lst_themes

def Find_Installed_SDDM_Themes():
	# return a list
	lst_dm = Get_Packages_Installed_Providing( "sddm-theme" )
	return lst_dm

def Install_SDDM_Theme( p_str_Greeter ):
	# set nominated as current
	return False

def Impose_SDDM_Theme( p_str_Theme ):
	# set nominated as current
	return False

# Window Managers 
# not sure if this is meaningful separately from the DEs using them

# find those installed with:
# update-alternatives --list x-window-manager

# Desktop Environments - Ubuntu Specific for now

def Find_Current_DE_Session():
	# return a string
	crrnt_dm = os.getenv('DESKTOP_SESSION')
	return crrnt_dm

def Find_Available_DesktopEnvironments():
	# return a list
	lst_dm = [ "Ubuntu(Gnome3)", "Xubuntu", "Kubuntu", "Budgie" ]
	return lst_dm

def Install_DesktopEnvironments( p_str_DisplayManager ):
	# Install the nominated Desktop Environment - can then be selected at login
	if p_str_DisplayManager == "Ubuntu(Gnome3)" :
		print( "")
	did_ok = False
	return did_ok

if __name__ == "__main__" :
	#print( "Get_Packages_Installed_Providing x-display-manager" )
	#print( Get_Packages_Installed_Providing( "x-display-manager" ))
	#print( "Get_Packages_Installed_Providing .*lightdm.*greeter.*" )
	#print( Get_Packages_Installed_Providing( ".*lightdm.*greeter.*" ))
	#print( "Get_Packages_Installed_Providing sddm-theme" )
	#print( Get_Packages_Installed_Providing( "sddm-theme" ))
	#
	print( "? Find_Current_LightDM_Greeter(): " + Find_Current_LightDM_Greeter())
	print( "? Find_Available_LightDM_Greeters(): " )
	print( Find_Available_LightDM_Greeters())
	print( "? Find_Installed_LightDM_Greeters(): " )
	print( Find_Installed_LightDM_Greeters())
	#
	print( "? Find_Current_SDDM_Theme(): " + Find_Current_SDDM_Theme())
	print( "? Find_Available_SDDM_Themes(): " )
	print( Find_Available_SDDM_Themes())
	print( "? Find_Installed_SDDM_Themes(): " )
	print( Find_Installed_SDDM_Themes())
	#
	print( "? Find_Current_DE_Session(): " + Find_Current_DE_Session())
	print( "? Find_Available_DesktopEnvironments(): " )
	print( Find_Available_DesktopEnvironments())

